import { AuthService } from './auth.service';
import { User } from './../../login/user';
import { Injectable } from '@angular/core';
import { RequestLogin } from '../models/RequestLogin';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ResponseLogin } from '../models/ResponseLogin';
import { environment } from '../../../environments/environment';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  apiURL: string = environment.apiURLBase ;
  constructor
  (
    private http: HttpClient,
    private authService: AuthService
  ) 
  { }

  salvar(user: User): Observable<any>{
    return this.http.post<any>(this.apiURL + '/api/register', user);
  }
  login(requestLogin: RequestLogin):Observable<ResponseLogin>{
    return this.http.post<ResponseLogin>
    (
      this.apiURL + '/api/login', requestLogin
    )
    .pipe(
      tap((loginResponse) => (this.authService.loginResponse = loginResponse))
    );
  }
}
