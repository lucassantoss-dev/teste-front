import { Observable } from 'rxjs';
import { Parceiro } from './../../parceiros/parceiros';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParceirosService {
  apiURL: string = environment.apiURLBase + '/api/parceiros';

  constructor(private http: HttpClient) { }
  salvar(parceiro: Parceiro): Observable<Parceiro>{
    return this.http.post<Parceiro>(`${this.apiURL}`, parceiro)
  }
  atualizar(parceiro: Parceiro): Observable<any>{
    return this.http.put<Parceiro>(`${this.apiURL}/${parceiro.id}`, parceiro)
  }
  
  getParceiros(): Observable<Parceiro[]> {
    return this.http.get<Parceiro[]>(this.apiURL);
  }
  getParceiroById(id: number) : Observable<Parceiro>{
    return this.http.get<any>(`${this.apiURL}/${id}`);
  }
  deletar(parceiro: Parceiro): Observable<any>{
    return this.http.delete<Parceiro>(`${this.apiURL}/${parceiro.id}`)
  }
}
