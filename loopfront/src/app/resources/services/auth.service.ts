import { Router } from '@angular/router';
import { ResponseLogin } from '../models/ResponseLogin';
// import { RequestLogin } from '../models/RequestLogin';
// import { Observable } from 'rxjs';
// import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { User } from '../../login/user';
// import { tap } from 'rxjs/operators';
// import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loginResponse: ResponseLogin;
  constructor(
    private router: Router
   ) { }

  clear(): void{
    this.loginResponse = undefined;
  }

  isAuthenticated(): boolean {
    return Boolean(this.loginResponse?.token);
  }
}
