import { ResponseLogin } from './../models/ResponseLogin';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardiaService implements CanActivate {

  loginResponse: ResponseLogin
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }
  canActivate(): boolean {
    localStorage.getItem('token');
    return this.authService.isAuthenticated();
  }
}
