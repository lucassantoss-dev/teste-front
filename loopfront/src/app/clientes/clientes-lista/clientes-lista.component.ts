import { AlertService } from './../../resources/services/alert.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CadastrosService } from './../../resources/services/cadastros.service';
import { Cliente } from './../cliente';


@Component({
  selector: 'app-clientes-lista',
  templateUrl: './clientes-lista.component.html',
  styleUrls: ['./clientes-lista.component.css']
})
export class ClientesListaComponent implements OnInit {

  clients: Cliente[] = [];
  clienteSelecionado: Cliente;
  mensagemSucesso: string;
  mensagemErro: string;

  constructor(
    private service: CadastrosService, 
    private router: Router,
    private alertService: AlertService
    ) { }

  ngOnInit(): void {
    this.service
    .getClientes()
    .subscribe
    (response => this.clients = response);
  }

  novoCadastro(){
    this.router.navigate(['/clientes/form'])
  }
  preparaDelecao(cliente: Cliente){
    this.clienteSelecionado = cliente;
  }
  deletarCliente(){
    this.service
    .deletar(this.clienteSelecionado)
    .subscribe( response => {
      this.mensagemSucesso = 'Cliente deletado com sucesso!'
      this.ngOnInit();
    },
    erro => this.mensagemErro = 'Ocorreu um erro ao deletar o cliente!')
  }

}
