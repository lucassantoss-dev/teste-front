import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaskModule } from 'ngx-mask'

import { ParceirosRoutingModule } from './parceiros-routing.module';
import { ParceirosFormComponent } from './parceiros-form/parceiros-form.component';
import { ParceirosListaComponent } from './parceiros-lista/parceiros-lista.component';




@NgModule({
  declarations: [ParceirosFormComponent, ParceirosListaComponent],
  imports: [
    CommonModule,
    ParceirosRoutingModule,
    FormsModule,
    NgxMaskModule.forChild()
  ], 
  exports: [
    ParceirosFormComponent,
    ParceirosListaComponent
  ]
})
export class ParceirosModule { }
