import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BeneficiosRoutingModule } from './beneficios-routing.module';
import { BeneficiosFormComponent } from './beneficios-form/beneficios-form.component';
import { BeneficiosListaComponent } from './beneficios-lista/beneficios-lista.component';


@NgModule({
  declarations: [
    BeneficiosFormComponent, 
    BeneficiosListaComponent,
  ],
  imports: [
    CommonModule,
    BeneficiosRoutingModule,
    FormsModule, 
    RouterModule
  ], exports: [
    BeneficiosFormComponent, 
    BeneficiosListaComponent
  ]
})
export class BeneficiosModule { }
