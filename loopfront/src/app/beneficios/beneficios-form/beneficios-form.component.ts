import { Beneficios } from './../beneficios';
import { Cliente } from './../../clientes/cliente';
import { Component, OnInit } from '@angular/core';
import { CadastrosService } from '../../resources/services/cadastros.service';

@Component({
  selector: 'app-beneficios-form',
  templateUrl: './beneficios-form.component.html',
  styleUrls: ['./beneficios-form.component.css']
})
export class BeneficiosFormComponent implements OnInit {

  clients: Cliente[] = []
  beneficios: Beneficios;

  constructor(
    private cadastrosService: CadastrosService
  ) { 
    this.beneficios = new Beneficios();
  }

  ngOnInit(): void {
    this.cadastrosService
    .getClientes()
    .subscribe(response => this.clients = response)
  }
  onSubmit(){
    console.log(this.beneficios);
  }

}
